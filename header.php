<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MEUBLE
 */
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="robots" content="noindex, follow">
    <title>
		<?php
		bloginfo('name');
		echo ' | ';
        if (wp_title('', false)) {
			echo "";
        } else {
            echo bloginfo('description');
        } wp_title('');
		?>
	</title>
    <link rel="icon" href="#">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if( is_singular() && pings_open( get_queried_object() ) ): ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<script src="<?php echo get_template_directory_uri()?>/assets/js/jquery.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php 
	if ( is_front_page() ) {
		?>
			<style>
				.main-header{
					/* background-color: transparent; */
					background-color: #fff;
				}
			</style>
		<?php
	} else{
		?>
			<style>
				.main-header{
					background-color: #ffffff;
				}
			</style>
			<script>
				$(document).ready(function(){
					$("main").css({
						paddingTop: $(".main-header").innerHeight()+'px'
					})
					
					$(window).on("resize",function(){
						$("main").css({
							paddingTop: $(".main-header").innerHeight()+'px'
						})
					})
				});
			</script>
		<?php
	}
?>

<header class="main-header">
	<div class="navbar">
		<a href="<?php bloginfo('url'); ?>" class="nav-item">
			<div class="logo">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/logo.svg" alt="meuble logo">
			</div>
		</a>
		<div class="nav-menu">
			<?php 
				wp_nav_menu(
					array (
						'theme_location' => 'primary'
						)
					); 
			?>
		</div>
		<span class="hamburger hamburger-collapse" id="navbar-toggle">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</span>
	</div>
</header>

<main>
 
