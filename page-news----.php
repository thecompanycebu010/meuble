<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MEUBLE
 */

get_header();
?>

<!-- news banner -->
<section class="news-banner">
    <div class="m-tit">
        <h2>News</h2>
        <p>最新情報</p>
    </div>
    <div class="m-breadcrumbs">
        <ul>
            <li><a href="#">Home</a></li>
            <li>News</li>
        </ul>
    </div>
</section>
<!-- end of news banner -->

<!-- news detail -->
<section class="news-details">
    <div class="news-detail-cntr">
        <div class="gap gap-40 gap-0-xs">
            <div class="md-9 xs-12">
                <ul class="news-list-detail">
                    <li class="news-list-detail-item">
                        <a href="#">
                            <div class="news-text-cont">
                                <span class="news-text-det">コーポレートサイトが新しくなりました</span><span class="news-det-tag">New</span>
                            </div>
                            <div class="news-date">
                                <span class="news-det-date">2020 4.1</span> / <span class="news-det-tag">カテゴリ名</span>
                            </div>
                        </a>
                    </li>
                    <li class="news-list-detail-item">
                        <a href="#">
                            <div class="news-text-cont">
                                <span class="news-text-det">コーポレートサイトが新しくなりました</span>
                            </div>
                            <div class="news-date">
                                <span class="news-det-date">2020 4.1</span> / <span class="news-det-tag">カテゴリ名</span>
                            </div>
                        </a>
                    </li>
                    <li class="news-list-detail-item">
                        <a href="#">
                            <div class="news-text-cont">
                                <span class="news-text-det">コーポレートサイトが新しくなりました</span>
                            </div>
                            <div class="news-date">
                                <span class="news-det-date">2020 4.1</span> / <span class="news-det-tag">カテゴリ名</span>
                            </div>
                        </a>
                    </li>
                    <li class="news-list-detail-item">
                        <a href="#">
                            <div class="news-text-cont">
                                <span class="news-text-det">コーポレートサイトが新しくなりました</span>
                            </div>
                            <div class="news-date">
                                <span class="news-det-date">2020 4.1</span> / <span class="news-det-tag">カテゴリ名</span>
                            </div>
                        </a>
                    </li>
                    <li class="news-list-detail-item">
                        <a href="#">
                            <div class="news-text-cont">
                                <span class="news-text-det">コーポレートサイトが新しくなりました</span>
                            </div>
                            <div class="news-date">
                                <span class="news-det-date">2020 4.1</span> / <span class="news-det-tag">カテゴリ名</span>
                            </div>
                        </a>
                    </li>
                    <li class="news-list-detail-item">
                        <a href="#">
                            <div class="news-text-cont">
                                <span class="news-text-det">コーポレートサイトが新しくなりました</span>
                            </div>
                            <div class="news-date">
                                <span class="news-det-date">2020 4.1</span> / <span class="news-det-tag">カテゴリ名</span>
                            </div>
                        </a>
                    </li>
                    <li class="news-list-detail-item">
                        <a href="#">
                            <div class="news-text-cont">
                                <span class="news-text-det">コーポレートサイトが新しくなりました</span>
                            </div>
                            <div class="news-date">
                                <span class="news-det-date">2020 4.1</span> / <span class="news-det-tag">カテゴリ名</span>
                            </div>
                        </a>
                    </li>
                    <li class="news-list-detail-item">
                        <a href="#">
                            <div class="news-text-cont">
                                <span class="news-text-det">コーポレートサイトが新しくなりました</span>
                            </div>
                            <div class="news-date">
                                <span class="news-det-date">2020 4.1</span> / <span class="news-det-tag">カテゴリ名</span>
                            </div>
                        </a>
                    </li>
                    <li class="news-list-detail-item">
                        <a href="#">
                            <div class="news-text-cont">
                                <span class="news-text-det">コーポレートサイトが新しくなりました</span>
                            </div>
                            <div class="news-date">
                                <span class="news-det-date">2020 4.1</span> / <span class="news-det-tag">カテゴリ名</span>
                            </div>
                        </a>
                    </li>
                    <li class="news-list-detail-item">
                        <a href="#">
                            <div class="news-text-cont">
                                <span class="news-text-det">コーポレートサイトが新しくなりました</span>
                            </div>
                            <div class="news-date">
                                <span class="news-det-date">2020 4.1</span><span class="news-det-tag">カテゴリ名</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="md-3 xs-12">
                <div class="m-category-wrp">
                    <h4>Category</h4>
                    <ul class="news-cat-list">
                        <li class="active">
                            <a href="#">All</a>
                        </li>
                        <li>
                            <a href="#">Meuble</a>
                        </li>
                        <li>
                            <a href="#">Literie</a>
                        </li>
                        <li>
                            <a href="#">IKASAS</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="news-det-btn">
            <a href="#">新しい記事へ</a>
            <a href="#">新しい記事へ</a>
        </div>
    </div>
</section>
<!-- end of news detail -->

<!-- cs 4th -->
<?php get_template_part("template-parts/recruit-temp");?>
<!-- //cs 4th -->

<!-- contact -->
<?php get_template_part("template-parts/contact-temp");?>
<!-- //contact -->

<?php
get_footer();