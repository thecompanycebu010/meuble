<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MEUBLE
 */

get_header();
?>

<!-- news banner -->
<section class="news-banner">
    <div class="m-tit">
        <h2>News</h2>
        <p>最新情報</p>
    </div>
    <div class="m-breadcrumbs">
        <ul>
            <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
            <li>News</li>
        </ul>
    </div>
</section>
<!-- end of news banner -->

<!-- news detail -->
<section class="news-details">
    <div class="news-detail-cntr">
        <div class="gap gap-40 gap-0-xs">
            <div class="md-8 xs-12">
                <div class="gap gap-30 gap-0-xs">
                    <div class="md-6 xs-12">
                        <div class="mn-nw-ttl">
                            <h3>企業からの最新情報</h3>
                            <ul class="news-list-detail">
                                <?php 
                                    $count = 0;
                                    $cs_pg = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                                    $max_post_page = 3;
                                    if( isset($post_category) && !empty($post_category) ) {
                                        $query_cs = new WP_Query(
                                            array(
                                                'post_type'     =>'news', 
                                                'post_status'   =>'publish', 
                                                'posts_per_page'=> $max_post_page,
                                                'orderby'        => 'publish_date',
                                                'order'         => 'DESC',
                                                'paged'         => $cs_pg,
                                                'tax_query' => [
                                                    [
                                                        'taxonomy' => 'news-category',
                                                        'field' => 'slug',
                                                        'terms' => $post_category,
                                                        'order' => 'DESC',
                                                        'include_children' => true,
                                                        
                                                    ]
                                                ]
                                        )); 
                                        }else{
                                            $query_cs = new WP_Query(
                                                array(
                                                    'post_type'     =>'news', 
                                                    'post_status'   =>'publish', 
                                                    'posts_per_page'=> $max_post_page,
                                                    'orderby'        => 'publish_date',
                                                    'order'         => 'DESC',
                                                    'paged'         => $cs_pg
                                            )); 
                                        }
                                    
                                        $total_pages = $query_cs->max_num_pages;
                                        $stat_class = "";
                                        $cat_n = "";
                                        if ( $query_cs->have_posts() ) :
                                                
                                        while ( $query_cs->have_posts() ) : $query_cs->the_post(); $count++; 
                                        $categories = get_the_terms( $post->ID, 'news-category' );
                                        $cat_name = $categories[0]->name;
                                ?>
                                <li class="news-list-detail-item" data-aos="fade-up" data-aos-duration="2000">
                                    <a href="<?php the_permalink(); ?>">
                                        <ul class="mn-nw-lst">
                                            <li>
                                                <?php if(has_post_thumbnail()) : ?>
                                                <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="is-wide" />
                                                <?php else: ?>
                                                <img src="https://dummyimage.com/600x400/3b3a3b/ffffff.png&text=No+Image" alt="<?php the_title(); ?>" class="is-wide">
                                                <?php endif; ?>
                                            </li>
                                            <li>
                                                <div class="news-text-cont">
                                                    <span class="news-text-det"><?php the_title(); ?></span>
                                                </div>
                                                <div class="news-date">
                                                    <span class="news-det-date"><?php the_time('Y m.d'); ?></span> / <span class="news-det-tag">
                                                        <?php echo $cat_name; ?>
                                                    </span><span class="mn-news-tag">New</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </a>
                                </li>
                                <?php endwhile; ?>
                                    <?php else: ?>
                                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                                <?php endif; ?>
                            </ul>
                            <div class="news-det-btn" data-aos="fade-up" data-aos-duration="2000">
                                <a href="#">企業からの最新情報一覧</a>
                            </div>
                        </div>    
                    </div>
                    <div class="md-6 xs-12">
                        <div class="mn-nw-ttl">
                            <h3>Meub l eの最新情報</h3>
                            <ul class="news-list-detail">
                                <?php echo do_shortcode('[myphp file="rss_feed" rss_url="http://preodesign7.xsrv.jp/meuble-brand/news/feed/"]'); ?>
                            </ul>
                        </div>
                    </div>
                    <div class="md-6 xs-12">
                        <div class="mn-nw-ttl">
                            <h3> L i t e r i eの最新情報</h3>
                            <ul class="news-list-detail">
                                <?php echo do_shortcode('[myphp file="rss_feed" rss_url="https://www.literie.jp/feed/"]'); ?>
                                <?php //echo do_shortcode('[myphp file="rss_feed" rss_url="http://jp.automaton.am/feed/"]'); ?>
                                <?php //echo do_shortcode('[myphp file="rss_feed" rss_url="http://kmga.jp/category/training/fun/feed/"]'); ?>
                            </ul>
                        </div>
                    </div>
                    <div class="md-6 xs-12">
                        <div class="mn-nw-ttl">
                            <h3>IKASASの最新情報</h3>
                            <ul class="news-list-detail">
                                <?php echo do_shortcode('[myphp file="rss_feed" rss_url="https://ikasas.jp/feed"]'); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="md-4 xs-12" data-aos="fade-up" data-aos-duration="2000">
                <div class="m-category-wrp is-a">
                    <h4>企業からの最新情報</h4>
                    <ul class="news-cat-list">
                        <li>
                            <a href="#">お知らせ</a>
                        </li>
                        <li>
                            <a href="#">CSR活動</a>
                        </li>
                        <li>
                            <a href="#">その他</a>
                        </li>
                    </ul>
                </div>
                <div class="m-category-wrp">
                    <h4>ブランドの最新情報</h4>
                    <ul class="news-cat-list">
                        <li>
                            <a href="#">Meuble<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico_01.png" alt="" class="is-wide"></span></a>
                        </li>
                        <li>
                            <a href="#">Literie<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico_01.png" alt="" class="is-wide"></span></a>
                        </li>
                        <li>
                            <a href="#">IKASAS<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico_01.png" alt="" class="is-wide"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- <div class="news-det-btn" data-aos="fade-up" data-aos-duration="2000">
            <a href="#">新しい記事へ</a>
            <a href="#">新しい記事へ</a>
        </div> -->
    </div>
</section>
<!-- end of news detail -->

<!-- cs 4th -->
<?php get_template_part("template-parts/recruit-temp");?>
<!-- //cs 4th -->

<!-- contact -->
<?php get_template_part("template-parts/contact-temp");?>
<!-- //contact -->

<?php
get_footer();