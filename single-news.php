<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MEUBLE
 */

get_header();
?>

<!-- news banner -->
<section class="news-banner">
    <div class="m-tit">
        <h2>News</h2>
        <p>最新情報</p>
    </div>
    <div class="m-breadcrumbs">
        <ul>
            <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
            <li><a href="<?=esc_url( home_url("/news") );?>">News</a></li>
            <li>「LINER / ライナー」の新商品情報を更新しました</li>
        </ul>
    </div>
</section>
<!-- end of news banner -->

<!-- news detail -->
<section class="news-details">
    <div class="news-detail-cntr">
        <div class="gap gap-40 gap-0-xs">
            <div class="md-8 xs-12">
                <div class="news-detail-cont">
                    <?php
                        $categories = get_the_terms( $post->ID, 'news-category' );
                        $cat_name = $categories[0]->name;
                    ?>
                    <div class="news-det-header">
                        <span class="news-det-cont-tag">New</span>
                        <h2><?php the_title(); ?></h2>
                        <span class="news-cont-date"><?php the_time('Y m.d'); ?></span> / <span class="news-cont-cat"><?php echo $cat_name; ?></span>
                    </div>
                    <?php 
                        $query_cs = new WP_Query(
                        array(
                            'taxonomy' => 'case_cat',
                            'post_type'     =>'case_study', 
                            'post_status'   =>'publish', 
                            'posts_per_page' => -1, 
                            'orderby'        => 'publish_date',
                            'order'         => 'DESC'
                        ));  
                    ?>
                    <div class="news-det-body">
                        <div class="news-cont-img">
                            <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/img/news/news-img01.png" alt="" class="is-wide"> -->
                            <?php if(has_post_thumbnail()) : ?>
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="is-wide" />
                            <?php else: ?>
                            <img src="https://dummyimage.com/600x400/3b3a3b/ffffff.png&text=No+Image" alt="<?php the_title(); ?>" class="is-wide">
                            <?php endif; ?>
                        </div>
                        <div class="news-cont-txt">
                            <p class="m-desc">
                                <?php
                                    while ( have_posts() ) :
                                        the_post();
                                        the_content();
                                    endwhile; // End of the loop.
                                ?>
                            </p>
                        </div>
                       
                    </div>
                    <div class="news-det-footer">
                        <ul class="news-soc-links">
                            <li>Share</li>
                            <li>
                                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-fb.svg" alt="">Facebook</a>
                            </li>
                            <li>
                                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-twit.svg" alt="">Twitter</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="md-4 xs-12">
                <div class="m-category-wrp is-a">
                    <h4>企業からの最新情報</h4>
                    <ul class="news-cat-list">
                        <li>
                            <a href="#">お知らせ</a>
                        </li>
                        <li>
                            <a href="#">CSR活動</a>
                        </li>
                        <li>
                            <a href="#">その他</a>
                        </li>
                    </ul>
                </div>
                <div class="m-category-wrp">
                    <h4>ブランドの最新情報</h4>
                    <ul class="news-cat-list">
                        <li>
                            <a href="#">Meuble<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico_01.png" alt="" class="is-wide"></span></a>
                        </li>
                        <li>
                            <a href="#">Literie<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico_01.png" alt="" class="is-wide"></span></a>
                        </li>
                        <li>
                            <a href="#">IKASAS<span><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico_01.png" alt="" class="is-wide"></span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="news-det-btn">
            <a href="#">NEWS一覧へ</a>
        </div>
    </div>
</section>
<!-- end of news detail -->

<!-- cs 4th -->
<?php get_template_part("template-parts/recruit-temp");?>
<!-- //cs 4th -->

<!-- contact -->
<?php get_template_part("template-parts/contact-temp");?>
<!-- //contact -->

<?php
get_footer();