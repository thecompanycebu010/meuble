<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MEUBLE
 */

get_header();
?>
<style>
#back_contact {
    display: none;
}
</style>

<div class="contact-page">
    <h2 class="m-contact-title">Contact <span>株式会社モーブル へのお問い合わせ</span></h2>

    <div class="m-breadcrumbs">
        <ul>
            <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
            <li>Contact</li>
        </ul>
    </div>

    <div class="contact-container">
        <div class="contact-text">
            <h3>協業のご相談や弊社に関するお問い合わせは
            <span>以下のお問い合わせフォームよりどうぞ。</span></h3>
            <p>御社の強みと弊社の強みを掛け合わせ、お互いに高め合い、新しい価値を生み出しませんか？</p>
            <p>熱い想いをお持ちの企業様からのお問い合わせを募集しております！</p>
        </div>
        <div class="prod-inqry">
            <div class="box">
                <p>商品へのお問い合わせはこちら</p>
                <ul class="mn-prd-lst">
                    <li>
                        <a href="#">>Meuble 商品のお問い合わせ</a>
                    </li>
                    <li>
                        <a href="#">>Literie商品のお問い合わせ</a>
                    </li>    
                    <li>
                        <a href="#">>IKASAS商品のお問い合わせ</a>
                    </li>    
                </ul>
            </div>
        </div>

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                the_content();
                
        endwhile; else: ?>
        <p>Sorry, no posts matched your criteria.</p>
        <?php endif; ?>
</div>

<?php
get_footer(); 
?>

<script>
$(function () {
    // 初期選択状態(個人)
    $('input[name="phone-number"]').val('0000000');
    $('input[name="address"]').val('個人が選択されたので省略されました。');
    $('input[name="phone-number2"]').val('0000000000');
    $(".contact-fields").each(function(i, elem) {
        if(i==7 || i==8 || i==9){
            //console.log(i + ': ' + $(elem).text());
            $(elem).hide();
        }
    });
    // ラジオボタンを選択変更したら実行
    $('input[name="about-you"]').change(function () {

        // value値取得
        var val = $(this).val();
        // コンソールログで確認
        //console.log(val);

        if(val=="個人"){
            // ダミーの値をセット
            $('input[name="phone-number"]').val('0000000');
            $('input[name="address"]').val('個人が選択されたので省略されました。');
            $('input[name="phone-number2"]').val('0000000000');

            $(".contact-fields").each(function(i, elem) {
                if(i==7 || i==8 || i==9){
                    //console.log(i + ': ' + $(elem).text());
                    $(elem).hide();
                }
            });
        }else{
            // 値をクリア
            $('input[name="phone-number"]').val('');
            $('input[name="address"]').val('');
            $('input[name="phone-number2"]').val('');

            $(".contact-fields").each(function(i, elem) {
                if(i==7 || i==8 || i==9){
                    //console.log(i + ': ' + $(elem).text());
                    $(elem).show();
                }
            });
        }

    });
});
</script>