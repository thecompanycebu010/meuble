<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage MEUBLE 
 * @since MEUBLE 1.0
 */
get_header();
?>

    <!-- cs-1st -->
    <section class="m-cs_hero-wrp is-aboutus">
        <div class="m-cs_inner">
            <div>
                <h2 class="m-cs_title">ABOUT US
                    <span>会社概要</span>
                </h2>
            </div>
        </div>
        <!-- breadcrumbs -->
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
                <li><a href="<?=esc_url( home_url("company-profile") );?>">COMPANY</a></li>
                <li>ABOUT US</li>
            </ul>
        </div>
        <!-- //breadcrumbs -->
    </section>
    <!-- //cs 1st -->
    
    <!-- cs-2nd -->
    <section class="cntr about-wrp mn-ovrhdn">
        <div class="mn-box8">
            <h4 class="mn-line-btm" data-aos="fade-up" data-aos-duration="2000">
                <span>会社概要</span>
            </h4>    
            <div class="dl-aboutus">
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>社名</dt>
                    <dd>株式会社モーブル</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>代表者</dt>
                    <dd>坂田 道亮</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>本社所在地</dt>
                    <dd>〒831-0006　福岡県大川市中古賀956-1</dd>
                </dl data-aos="fade-up" data-aos-duration="2000">
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>電話番号</dt>
                    <dd>0944-88-1955(代)</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>FAX番号</dt>
                    <dd>0944-88-3068</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>東京日本橋オフィス所在地</dt>
                    <dd>〒101-0032　東京都千代田区岩本町1-8-16アチェロビル4F</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>電話番号</dt>
                    <dd>03-5823-4115</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>FAX番号</dt>
                    <dd>03-5823-4116</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>メールアドレス</dt>
                    <dd>nfo@meuble.co.jp</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>URL</dt>
                    <dd>http://www.meuble.co.jp/</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>創業</dt>
                    <dd>昭和61（1986）年　9月2日</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>従業員数</dt>
                    <dd>60名　（2019年8月現在）</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>事業内容</dt>
                    <dd>
                        オリジナル家具の企画・製造・販売<br>
                        特許を使用したオリジナルマットレスの企画・製造・販売・OEM生産<br>
                        「IKASAS」国内販売総代理店
                    </dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>資本金</dt>
                    <dd>4000万円</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>主な取引先</dt>
                    <dd>全国有名家具専門店・家具取扱店・ハウスメーカー・全国宿泊施設・ホテル</dd>
                </dl>
                <dl data-aos="fade-up" data-aos-duration="2000">
                    <dt>関連会社</dt>
                    <dd>
                        モリタインテリア工業株式会社<br>
                        〒830-0404<br>
                        福岡県三潴郡大木町横溝500
                    </dd>
                </dl>
            </div>
        </div>
        <div class="mn-box10">
            <div class="access">
                <h4 class="mn-line-btm" data-aos="fade-up" data-aos-duration="2000">
                    <span>アクセス</span>
                </h4>   
                <div class="gap gap-40 gap-10-xs">
                    <div class="md-6 xs-12 mb-40-xs">
                        <div class="office" data-aos="fade-up" data-aos-duration="2000">
                            <h3>福岡本社所在地</h3>
                        </div>
                        <div class="map" data-aos="fade-up" data-aos-duration="2000">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about/map_01.png" alt="" class="is-wide">
                        </div>
                        <div class="location" data-aos="fade-up" data-aos-duration="2000">
                            <p>〒831-0006　福岡県大川市中古賀956-1（大川家具工業団地の隣）</p>
                            <a href="#">
                                <p>（>googlemap）</p>
                            </a>
                        </div>
                        <div class="contents" data-aos="fade-up" data-aos-duration="2000">
                            <p>
                                九州自動車道：八女ICより車で40分<br>
                                長崎自動車道：東脊振ICより車で30分
                            </p>
                        </div>    
                    </div>
                    <div class="md-6 xs-12 mt-30-xs mb-20-xs">
                        <div class="office" data-aos="fade-up" data-aos-duration="2000">
                            <h3>東京日本橋オフィス所在地</h3>
                        </div>
                        <div class="map" data-aos="fade-up" data-aos-duration="2000">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about/map_02.png" alt="" class="is-wide">
                        </div>
                        <div class="location" data-aos="fade-up" data-aos-duration="2000">
                            <p>〒101-0032　東京都千代田区岩本町1-8-16アチェロビル4F</p>
                            <a href="#">
                                <p>（>googlemap）</p>
                            </a>
                        </div>
                        <div class="contents" data-aos="fade-up" data-aos-duration="2000">
                            <p>
                                東京オフィスへのアクセス方法が入ります<br>
                                東京オフィスへのアクセス方法が入ります
                            </p>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
        <div class="mn-wrp-m is-a">
            <div class="know">
                <h4 class="mn-line-btm" data-aos="fade-up" data-aos-duration="2000">
                    <span>もっとモーブル を知る</span>
                </h4>   
                <ul class="know-lst">
                    <li class="is-abt">
                        <a href="#">
                            <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb01.png" alt="" class="is-wide">
                                <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                                    <h3>代表挨拶<small>MESSAGE</small></h3>
                                <div>
                            </div>
                        </a>
                    </li>
                    <li class="is-abt">
                        <a href="#">
                            <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb02.png" alt="" class="is-wide">
                                <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                                    <h3>経営理念<small>PHILOSOPHY</small></h3>
                                <div>
                            </div>
                        </a>
                    </li>
                    <li class="is-abt">
                        <a href="#">
                            <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb03.png" alt="" class="is-wide">
                                <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                                    <h3>企業活動<small>CSR,SDGs</small></h3>
                                <div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- //cs-2nd -->

    <!-- cs 5th -->
    <?=get_template_part("template-parts/recruit-temp");?>
    <!-- //cs 5th -->

    <!-- contact -->
    <?=get_template_part("template-parts/contact-temp");?>
    <!-- //contact -->

<?php
get_footer();
?>