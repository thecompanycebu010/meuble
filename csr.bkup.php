<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage MEUBLE 
 * @since MEUBLE 1.0
 */
get_header();
?>

    <!-- cs-1st -->
    <section class="m-cs_hero-wrp is-csr">
        <div class="m-cs_inner">
            <div>
                <h2 class="m-cs_title">CSR,SDGs
                    <span>企業活動</span>
                </h2>
            </div>
        </div>
        <!-- breadcrumbs -->
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
                <li><a href="<?=esc_url( home_url("company-profile") );?>">COMPANY</a></li>
                <li>CSR,SDGs</li>
            </ul>
        </div>
        <!-- //breadcrumbs -->
    </section>
    <!-- //cs 1st -->
    
    <!-- cs-2nd -->
    <section class="csr-wrp mn-ovrhdn">
        <div class="mn-box8">
            <h4 class="mn-line-btm" data-aos="fade-up" data-aos-duration="2000">
                <span>SDGsについて</span>
            </h4>    
            <div class="sec1">
                <h3>持続可能な世界のためにモーブル ができること。</h3>
                <p>
                SDGs（エスディージーズ：Sustainable Development Goals　持続可能な開発目標）とは、持続可能な世界を実現するための17のゴール・169のターゲットから構成されており、地球上の「誰一人として取り残さない」ことを誓っている国際目標です。
                </p>
            </div>
            <div class="sec2">
                <ul class="sec2-lst">
                    <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/txt_01.png" alt="" class="is-wide">
                    </li>
                    <li>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/img_01.png" alt="" class="is-wide">
                    </li>
                </ul>
                <div class="sec2-a">
                    <p>株式会社モーブル は持続可能な開発目標（SDGs）を支援しています</p>
                </div>
            </div>
            <div class="sec3">
                <h3>
                    モーブルはSDGsの17の目標のうち<br>以下の４つに力を入れて取り組んでいます
                </h3>
                <div class="sec3-a">
                    <div class="gap gap-10 gap-0-xs">
                        <div class="md-3 xs-6">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/img_02.png" alt="" class="is-wide">
                        </div>
                        <div class="md-3 xs-6">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/img_03.png" alt="" class="is-wide">
                        </div>
                        <div class="md-3 xs-6">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/img_04.png" alt="" class="is-wide">
                        </div>
                        <div class="md-3 xs-6">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/img_05.png" alt="" class="is-wide">
                        </div>
                    </div>
                </div>
                <div class="csr-lst">
                    <ul class="sec3-lst">
                        <li>一、マイクロプラスチックを出しません</li>
                        <li>一、バイオマスプラスチックの研究開発に取り組んでいます</li>
                        <li>一、CO2削減に取り組んでいます</li>
                        <li>一、有害なガスを出しません</li>
                        <li>一、100%リサイクル可能な素材を使用しています</li>
                    </ul>
                </div>
            </div>
            <div class="sec4">
                <h4 class="mn-line-btm" data-aos="fade-up" data-aos-duration="2000">
                    <span>CSR活動について</span>
                </h4>    
                <div class="mn-box965">
                    <div class="csr-box">
                        <h3>地元・大川市の保育園にLiterieマットを寄贈いたしました。</h3>
                        <p>大川中央保育園様</p>
                        <div class="slider-lists">
                            <div class="slide-list-item ov-hidden">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="base-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_01.png" alt="" class="is-wide">
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="slider-imgs owl-carousel owl-theme">
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_01.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_02.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_03.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_04.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_05.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_06.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_07.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_08.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_09.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_10.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="cntnts">
                            <p>【内容】</p>
                            <p>
                                園児さんとサポートして下さる保育スタッフの皆さんへ、お昼寝マットやクッションなど<br>合計５０点のリテリー商品を寄贈
                            </p>
                        </div>
                        <div class="cntnts is-a">
                            <p>【背景】</p>
                            <p>
                                地方の人口減と少子化問題が進む昨今、本社所在地である福岡県大川市も例外ではない。<br>ここ数十年の間、毎年高齢者は増えていき、子どもは減り続けているのが現状。<br>大川市でモノづくりを生業とする当社でも、「何か少しでも地域に貢献できることがないか」という想いで実現<br>「洗える」「ムレない」「へたりにくい」という特徴が成長期の子どもに最適で、また、近年多い新生児のうつぶせ寝による窒息死を防ぐ「沈み込みにくい高反発性」「90%以上の空気層からなる通気性」を有している商品です。<br>「90%以上の空気層からなる通気性」を有している
                            </p>
                        </div>
                        <div class="csr-lnk">
                            <a href="">>ニュース記事を見る</a>
                        </div>
                    </div>
                    <div class="csr-box">
                        <h3>屋久島地杉バス停プロジェクトにてベンチに置くクッション材を提供</h3>
                        <p>屋久島地杉プロジェクト様</p>
                            <div class="slide-list-item ov-hidden">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="base-img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_11.png" alt="" class="is-wide">
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="slider-imgs owl-carousel owl-theme">
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_11.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_12.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_13.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_14.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_07.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_08.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_09.png">
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="cntnts">
                            <p>【内容】</p>
                            <p>
                                屋久島地杉プロジェクトに対し、ベンチの上にクッション材を提供をするという形で協力。<br>バス停を使う利用者（島の子どもや観光客）がより快適に過ごせるようになった。
                            </p>
                        </div>
                        <div class="cntnts is-a">
                            <p>【背景】</p>
                            <p>
                                屋久島地杉プロジェクトの発端は、バス停で待つ人が安心して雨風を防げる場所が欲しかったのがひとつ。<br>
                                樹齢1000年を超える杉を「屋久杉」と呼ぶのに対し、戦後人の手によって植林した杉を「地杉」と呼び、地杉が間伐のタイミングに来ている。「保護するべき自然(屋久杉)」と「産業として活用するべき自然(地杉)」を考え、地杉の活用を見出し、家具や建材にすることによって木としての価値を上げることを目指すもの。
                            </p>
                        </div>
                        <div class="cntnts is-b">
                            <p>【前提】</p>
                            <p>
                                武蔵野美術大学の非常勤講師である建築家 アンドレア彦根先生指揮のもと、<br>
                                建築を学ぶ学生とともに、これまでの林業とこれからの林業について学びながら、<br>
                                エネルギー・森・木を結び付けられるシンボルとなるバス停の設計・デザインに取り組ん<br>だものである。
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mn-wrp-m is-a">
            <div class="know">
                <h4 class="mn-line-btm" data-aos="fade-up" data-aos-duration="2000">
                    <span>もっとモーブル を知る</span>
                </h4>   
                <ul class="know-lst">
                    <li class="is-abt">
                        <a href="#">
                            <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb01.png" alt="" class="is-wide">
                                <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                                    <h3>代表挨拶<small>MESSAGE</small></h3>
                                <div>
                            </div>
                        </a>
                    </li>
                    <li class="is-abt">
                        <a href="#">
                            <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb02.png" alt="" class="is-wide">
                                <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                                    <h3>経営理念<small>PHILOSOPHY</small></h3>
                                <div>
                            </div>
                        </a>
                    </li>
                    <li class="is-abt">
                        <a href="<?=esc_url( home_url("about-us") );?>">
                            <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb04.png" alt="" class="is-wide">
                                <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                                    <h3>会社概要<small>ABOUT US</small></h3>
                                <div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- //cs-2nd -->

    <!-- cs 5th -->
    <?=get_template_part("template-parts/recruit-temp");?>
    <!-- //cs 5th -->

    <!-- contact -->
    <?=get_template_part("template-parts/contact-temp");?>
    <!-- //contact -->


<?php
get_footer();
?>