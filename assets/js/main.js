/*------------------------------
".Top" Color (if scroll >= 60)
------------------------------*/ 

$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 10) {
        $(".main-header").addClass("sticky");
    } else {
        $(".main-header").removeClass("sticky");
    }
});

/*------------------------------
Mobile Menu (toggleClass to activate transitions)
------------------------------*/
(function($){

    const navbar = $('#navbar-toggle')

    navbar.on("click",function(e){
        e.preventDefault()
        if( $(".nav-menu").hasClass("is-toggled")) {
            $(this).removeClass("is-toggled")
            $(".nav-menu").removeClass("is-toggled")
        }else{
            $(this).addClass("is-toggled")
            $(".nav-menu").addClass("is-toggled")
        }
       
        return false
    })

})(jQuery);

/*-----------------------------
Back to Top
-----------------------------*/
(function($){
    var toTop = $('.scroll-top');

        toTop.find("a").click(function(e){
            e.preventDefault()

            var speed = 500;
            $("html, body").animate({scrollTop:0}, speed)

            return false
        })
})(jQuery);


$(document).ready(function(){
    // Add smooth scrolling to all links
    $("a").on('click', function(event) {
  
      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();
  
        // Store hash
        var hash = this.hash;
  
        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 800, function(){
     
          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      } // End if
    });
  });

// IE background-attachment:fixed issue
if(navigator.userAgent.match(/MSIE 10/i) || navigator.userAgent.match(/Trident\/7\./) || navigator.userAgent.match(/Edge\/12\./)) {
    $('body').on("mousewheel", function () {
        event.preventDefault();
        var wd = event.wheelDelta;
        var csp = window.pageYOffset;
        window.scrollTo(0, csp - wd);
    });
}
/*------------------------------
SLIDER TOP PAGE
------------------------------*/
(function ($) {
    var m_top = $('#m-top-slider');
    m_top.owlCarousel({
            lazyLoad: false,
            center: false,
            items: 1,
            nav: false,
            margin: 0,
            padding: 0,
            autoplay: true,
            touchDrag: false,
            pullDrag: false,
            mouseDrag: false,
            loop: true,
            dots: false,
            animateOut: 'fadeOut',
            autoplayHoverPause: false,
            responsive : {
                // breakpoint from 0 up
                0 : {
                    items: 1
                },
                // breakpoint from 480 up
                480 : {
                    items: 1
                },
                // breakpoint from 768 up
                768 : {
                    items: 1
                }
            }
        });
})(jQuery);

(function($){

	$('.date-input').attr('id', 'datetimepicker');

	var $toTop = $('.scroll-top');

	$toTop.click(function(e){
		// e.preventDefault();

		var speed = 500;
		$("html, body").animate({scrollTop:0}, speed);

		return false;
	});

	$(window).scroll(function(){
	
		var scrollTop = $(window).scrollTop();
		
		if(scrollTop > $('.sec-hero').innerHeight() / 5 ) {
			$('.scroll-top').addClass('show');
		}else{
			$('.scroll-top').removeClass('show');
		}
			
	});

	var $nav_toggle = $('#toggle-nav'),
		$nav_main = $('.main-nav'),
		$toggle_count = 1;

	$nav_toggle.click(function(e){
		// e.preventDefault();

		$toggle_count++;

		if($toggle_count % 2 === 0) {
			$(this).addClass('is-on');
			$nav_main.slideDown();
		}else{
			$(this).removeClass('is-on');
			$nav_main.slideUp();
		}

		return false;
	});
	$(document).on('click', '.is-translated',function(e){
		// e.preventDefault();

		$nav_main.find('.is-translated').not($(this)).removeClass('is-toggled');
		$nav_main.find('.is-translated').not($(this)).find('.main-nav-children').slideUp();
		
		$(this).addClass('is-toggled');
		$(this).find('.main-nav-children').slideToggle();

	});

	$(document).on('click', '.is-toggled',function(e){
		// e.preventDefault();
		$(this).removeClass('is-toggled');
	});

	$(window).on('load resize',function(){
		if($(window).innerWidth() < 750) {
			// append span to parent li with class: has-sub
			$('.main-nav ul').find('.has-sub').each(function(index){
				$(this).prepend('<span class="spans-child"></span>');			
			});

			$('.spans-child').click(function(e){
				e.preventDefault();
					$(this).parents('.has-sub').find('.main-nav-children').slideToggle();
				return false;
			});

			$nav_main.find('ul li .has-sub.is-translated').click(function(e){
				// e.preventDefault();
				// $nav_main.find('.is-toggled').not($(this)).removeClass('is-toggled');
				$(this).addClass('is-toggled');
			});

			$nav_main.find('ul li .loc').click(function(e){
				// e.preventDefault();
				$nav_main.slideUp();
				$nav_toggle.removeClass('is-on');
			});
		}
	});


	// sliders

	var $slider_lists = $('.slider-lists'),
		$slider = $('#hero-slider'),
		$slider_img = $('.slider-imgs'),
		$slider_img_src = "",
		$slider_member = $('.members-slider'),
		$slider_insta = $('.instagram-slider'),
		$slider_es = $('.es-slider');

		$slider.owlCarousel({
			items: 1,
			lazyLoad: true,
			autoplay: true,
			dots: true,
			loop: true,
			margin:0
		});	

		$slider_img.children().each( function( index ) {
			$(this).attr( 'data-position', index );
		});

		$slider_img.owlCarousel({
			items: 3,
			lazyLoad: true,
			dots: true,
			autoplay: 5000,
			touchDrag: true,
			pullDrag: true,
			mouseDrag: true,
			autoplayHoverPause:true,
			loop: true,
			center: true,
			margin: 0
		});

		$slider_lists.find('.slide-list-item').each(function(index){
			$(this).find('.slider-imgs').attr('id', 'item_q-'+$(this).index());
		});

		for(var i = 0 ; i <= $slider_lists.find('.slide-list-item').length - 1; i++) {

			$('#item_q-'+i).find('.owl-item>div').click(function(){
				$slider_img_src = $(this).find('img').attr('src');
				$(this).parents('.card').find('.base-img>img').attr('src', $slider_img_src);
				$(this).trigger('to.owl.carousel', $(this).data( 'position' ) ); 
			});

			$('#item_q-'+i).on('changed.owl.carousel',function(property){
				var current = property.item.index;
				var src = $(property.target).find(".item").eq(current).find("img").attr('src');
				$(this).parents('.card').find('.base-img>img').attr('src', src);
			});
		}		

		$slider_member.owlCarousel({
			items: 6,
			lazyLoad: true,
			dots: false,
			autoplay: true,
			touchDrag: true,
			pullDrag: true,
			mouseDrag: true,
			autoplayHoverPause: true,
			loop: true,
			center: false,
			margin: 8,
			responsive : {
				// breakpoint from 0 up
				0 : {
					items: 1
				},
				// breakpoint from 768 up
				751 : {
					items: 6
				}
			}
		});

		$slider_insta.owlCarousel({
			items: 6,
			lazyLoad: true,
			dots: false,
			autoplay: 1000,
			touchDrag: true,
			pullDrag: false,
			mouseDrag: true,
			autoplayHoverPause: true,
			loop: true,
			center: false,
			margin: 8,
			responsive : {
				// breakpoint from 0 up
				0 : {
					items: 1
				},
				// breakpoint from 768 up
				751 : {
					items: 6
				}
			}
		});

		$('.es-slider').owlCarousel({
			items: 1,
			lazyLoad:true,
			autoplay: true,
			dots: true,
			loop: true,
			autoplayHoverPause: true,
			margin:0
		});	

}) (jQuery);