<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage MEUBLE 
 * @since MEUBLE 1.0
 */
get_header();
?>
 
    <div class="mn-inner" data-aos="fade-up" data-aos-duration="2000">
        <div>
            <h2 class="mn-title">COMPANY
                <span>企業情報</span>
            </h2>
        </div>
    </div>
        
    <!-- breadcrumbs -->
    <div class="m-breadcrumbs">
        <ul>
            <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
            <li>COMPANY</li>
        </ul>
    </div>
    <!-- //breadcrumbs -->
    
    
    <div class="comp-cntr">
        <div class="comp-crd">
            <div class="image" data-aos="fade-up" data-aos-duration="2000">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/company/message.png" alt="" class="is-wide">
            </div>
            <div class="body">
                <div class="title" data-aos="fade-up" data-aos-duration="2000">MESSAGE</div>
                <div class="title2" data-aos="fade-up" data-aos-duration="2000">
                    <p>代表挨拶</p>
                </div>    
                <div class="cntnts">
                    <dl>
                        <dt data-aos="fade-up" data-aos-duration="2000">
                            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストストテキストテキストテキストテキスト</p>
                        </dt>
                        <dd data-aos="fade-up" data-aos-duration="2000">
                            <div class="btn-wrp">
                                <a href="#" class="mn-more-btn">READ MORE</a>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="comp-crd">
            <div class="image" data-aos="fade-up" data-aos-duration="2000">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/company/philosophy.png" alt="" class="is-wide">
            </div>
            <div class="body">
                <div class="title" data-aos="fade-up" data-aos-duration="2000">PHILOSOPHY</div>
                <div class="title2" data-aos="fade-up" data-aos-duration="2000">
                    <p>代表挨拶</p>
                </div>    
                <div class="cntnts">
                    <dl>
                        <dt data-aos="fade-up" data-aos-duration="2000">
                            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストストテキストテキストテキストテキスト</p>
                        </dt>
                        <dd data-aos="fade-up" data-aos-duration="2000">
                            <div class="btn-wrp">
                                <a href="#" class="mn-more-btn">READ MORE</a>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="comp-crd">
            <div class="image" data-aos="fade-up" data-aos-duration="2000">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/company/about.png" alt="" class="is-wide">
            </div>
            <div class="body">
                <div class="title" data-aos="fade-up" data-aos-duration="2000">ABOUT US</div>
                <div class="title2" data-aos="fade-up" data-aos-duration="2000">
                    <p>代表挨拶</p>
                </div>    
                <div class="cntnts">
                    <dl>
                        <dt data-aos="fade-up" data-aos-duration="2000">
                            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストストテキストテキストテキストテキスト</p>
                        </dt>
                        <dd data-aos="fade-up" data-aos-duration="2000">
                            <div class="btn-wrp">
                                <a href="<?=esc_url(home_url("/about-us"));?>" class="mn-more-btn">READ MORE</a>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="comp-crd">
            <div class="image" data-aos="fade-up" data-aos-duration="2000">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/company/csr.png" alt="" class="is-wide">
            </div>
            <div class="body">
                <div class="title" data-aos="fade-up" data-aos-duration="2000">CSR,SDGs</div>
                <div class="title2" data-aos="fade-up" data-aos-duration="2000">
                    <p>代表挨拶</p>
                </div>    
                <div class="cntnts" data-aos="fade-up" data-aos-duration="2000">
                    <dl>
                        <dt data-aos="fade-up" data-aos-duration="2000">
                            <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストストテキストテキストテキストテキスト</p>
                        </dt>
                        <dd data-aos="fade-up" data-aos-duration="2000">
                            <div class="btn-wrp">
                                <a href="<?=esc_url(home_url("/csr"));?>" class="mn-more-btn">READ MORE</a>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>


<?php get_template_part("template-parts/recruit-temp");?>

<?php get_template_part("template-parts/contact-temp");?>


<?php
get_footer();
?>