<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package TEMPLATE
 */

get_header();
?>

    <section class="wrp" id="pg_404">
        <div class="cntr">
            <div class="inner tc">
                <h1>404</h1>
                <h3>Page not found</h3>
                <p>Please try one of the following page below.</p>
                <a href="<?=esc_url(home_url("/"))?>" class="btn-back">Home Page</a>
            </div>
        </div>
    </section>

<?php
get_footer();