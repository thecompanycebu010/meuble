<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MEUBLE
 */

?>

	</main>
	<footer id="footer" class="main-footer">
		<div class="footer-cntr">
			<div class="footer-top">
				<div class="logo">
					<a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/logo-white.svg" alt="meuble logo"></a>
				</div>
				<ul class="mn-ftr-m">
					<li>
						<div class="footer-address">
							<p>【本社オフィス】-1</p>
							<p> 〒831-0006　福岡県大川市中古賀956-1　TEL：<a href="#" class="m-tel">0944-88-1955</a></p>
						</div>
					</li>
					<li>
						<div class="footer-address">
							<p>【東京日本橋オフィス】</p>
							<p>〒101-0032　東京都千代田区岩本町1-8-16アチェロビル4F　TEL：<a href="#" class="m-tel">03-5823-4115</a></p>
						</div>
					</li>
				</ul>
				<ul class="footer-links-pri">
					<li>
						<a href="#">プライバシーポリシー</a>
					</li>
					<li>
						<a href="#">サイトマップ</a>
					</li>
				</ul>
			</div>
			<div class="footer-bottom">
				<div class="copyright">
					<p>(C) MEUBLE</p>
				</div>
				<?php 
					wp_nav_menu(
						array (
							'theme_location' => 'secondary',
							'menu_class'      => 'footer-links-btm',
							)
						); 
				?>
			</div>
		</div>
		<div class="scroll-top">
			<a href="#">Top</a>
		</div>
	</footer>
<?php if(is_page('27')){?>
<script><?php //コンタクトフォーム?>
document.addEventListener( 'wpcf7mailsent', function( event ) {
   location = 'http://preodesign7.xsrv.jp/meuble/contact/thanks/';
}, false );
</script>
<?php }?>
	<?php wp_footer(); ?>
	<script>
		AOS.init({
			once: false,
		});
	</script>
	</body>
</html>