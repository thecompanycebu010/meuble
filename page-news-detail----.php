<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MEUBLE
 */

get_header();
?>

<!-- news banner -->
<section class="news-banner">
    <div class="m-tit">
        <h2>News</h2>
        <p>最新情報</p>
    </div>
    <div class="m-breadcrumbs">
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">News</a></li>
            <li>「LINER / ライナー」の新商品情報を更新しました</li>
        </ul>
    </div>
</section>
<!-- end of news banner -->

<!-- news detail -->
<section class="news-details">
    <div class="news-detail-cntr">
        <div class="gap gap-40 gap-0-xs">
            <div class="md-9 xs-12">
                <div class="news-detail-cont">
                    <div class="news-det-header">
                        <span class="news-det-cont-tag">New</span>
                        <h2>「LINER / ライナー」の新商品情報を更新しました</h2>
                        <span class="news-cont-date">2020 4.1</span> / <span class="news-cont-cat">カテゴリ名</span>
                    </div>
                    <div class="news-det-body">
                        <div class="news-cont-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/news/news-img01.png" alt="" class="is-wide">
                        </div>
                        <div class="news-cont-txt">
                            <p class="m-desc">
                                「LINER」の新商品情報をリビングボードページに更新しました。<br>
                                「LINER ／ ライナー」<br>
                                格子デザインの進化系。使いやすさも向上したミドルボードです。<br>
                                テレビサイズの大型化に伴い、取り扱いサイズもアップデートしました。<br>
                                などの文章が入ります。写真も文章も何点でも入力が可能です。
                            </p>
                        </div>
                    </div>
                    <div class="news-det-footer">
                        <ul class="news-soc-links">
                            <li>Share</li>
                            <li>
                                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-fb.svg" alt="">Facebook</a>
                            </li>
                            <li>
                                <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-twit.svg" alt="">Twitter</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="md-3 xs-12">
                <div class="m-category-wrp">
                    <h4>Category</h4>
                    <ul class="news-cat-list">
                        <li class="active">
                            <a href="#">All</a>
                        </li>
                        <li>
                            <a href="#">Meuble</a>
                        </li>
                        <li>
                            <a href="#">Literie</a>
                        </li>
                        <li>
                            <a href="#">IKASAS</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="news-det-btn">
            <a href="#">NEWS一覧へ</a>
        </div>
    </div>
</section>
<!-- end of news detail -->

<!-- cs 4th -->
<?php get_template_part("template-parts/recruit-temp");?>
<!-- //cs 4th -->

<!-- contact -->
<?php get_template_part("template-parts/contact-temp");?>
<!-- //contact -->

<?php
get_footer();