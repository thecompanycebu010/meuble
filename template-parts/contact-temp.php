<section class="m-cntct-wrp tc" data-aos="fade-up" data-aos-duration="2000">
    <div class="m-cntct-inner">
        <h2 class="m-cntct_title">CONTACT</h2>
        <h3 class="m-cntct_phar">モーブルへのご相談をお受けしております。<br>
        お気軽にお問い合わせください。</h3>
        <div class="m-line-btn_cntr">
            <a href="<?=esc_url(home_url("/contact"));?>" class="m-btn_line">CONTACT</a>
        </div>
    </div>
</section>