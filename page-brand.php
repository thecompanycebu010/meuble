<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MEUBLE
 */

get_header();
?>

<div class="clearfix">
    <div class="brand-page">
        <div class="m_banner brand_ban">
            <div class="m_page_title">
                <h2>Brand <span>モーブルの取扱ブランド</span></h2>
            </div>
        </div>
    
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
                <li>Brand</li>
            </ul>
        </div>
    </div>   <!-- end brand-page -->
    
        <div class="brand_middle_section">
            <div class="brand_ttle" data-aos="fade-up" data-aos-duration="1500">
                <span class="brand-line"><small></small></span>
                <h2>lineup</h2>
            </div>
            <div class="m-cards" data-aos="fade-up" data-aos-duration="2000">
                <a href="#" class="card-m1">
                    <div class="card-img one"></div>
                    <div class="card-content">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/brand/card-logo1.png" alt="card-logo1">
                        <p>自社オリジナルブランド</p>
                    </div>
                </a>
                <a href="#" class="card-m2">
                    <div class="card-img two"></div>
                    <div class="card-content">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/brand/card-logo2.png" alt="card-logo2">
                        <p>自社マットレスブランド</p>
                    </div>
                </a>
                <a href="#" class="card-m3">
                    <div class="card-img three"></div>
                    <div class="card-content">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/brand/card-logo3.png" alt="card-logo3">
                        <p>日本唯一の正規販売代理店</p>
                    </div>
                </a>
            </div>
        </div><!-- brand_middle_section -->
    
        <div class="brand-main-content">
            <div class="brand-box">
               <div class="brand-parent" data-aos="fade-up" data-aos-duration="2000">
                    <div class="brand-img one"></div>
                    <div class="br-box-con">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/brand/card-logo1.png" alt="card-logo1">
                        <h2>本当にくらしを豊かにできるモノづくりから生まれた<br>
                        自社ブランド「Meuble」（ダミー）</h2>
                        <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストストテキストテキストテキストテキスト</p>
                    </div>
               </div>
                <div class="m-line-btn_cntr" data-aos="fade-up" data-aos-duration="2000">
                    <a href="#" class="m-btn_line">ブランドサイトへ</a>
                </div>
            </div>
            <div class="brand-box">
                <div class="brand-parent" data-aos="fade-up" data-aos-duration="2000">
                    <div class="brand-img two"></div>
                    <div class="br-box-con">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/brand/card-logo2.png" alt="card-logo2">
                        <h2>“ Design your sleep. ”（ダミー）</h2>
                        <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストストテキストテキストテキストテキスト</p>
                    </div>
                </div>
                <div class="m-line-btn_cntr" data-aos="fade-up" data-aos-duration="2000">
                    <a href="#" class="m-btn_line">ブランドサイトへ</a>
                </div>
            </div>
            <div class="brand-box">
                <div class="brand-parent" data-aos="fade-up" data-aos-duration="2000">
                    <div class="brand-img three"></div>
                    <div class="br-box-con">
                        <img src="<?php bloginfo('template_url'); ?>/assets/img/brand/card-logo3.png" alt="card-logo3">
                        <h2>ブランドのキャッチコピーが入りますブランドのキャッ<br>チコピーが入ります</h2>
                        <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストストテキストテキストテキストテキスト</p>
                    </div>
                </div>
                <div class="m-line-btn_cntr" data-aos="fade-up" data-aos-duration="2000">
                    <a href="#" class="m-btn_line">ブランドサイトへ</a>
                </div>
            </div>
        </div> <!-- brand-main-content -->
        
        <div class="brand-page">
            <!-- cs 4th -->
            <?=get_template_part("template-parts/recruit-temp");?>
            <!-- //cs 4th -->
        </div>

        <!-- contact -->
        <?=get_template_part("template-parts/contact-temp");?>
        <!-- //contact -->

    
</div> <!-- end clearfix -->


<?php
get_footer();